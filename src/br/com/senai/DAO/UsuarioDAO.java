package br.com.senai.DAO;

import org.hibernate.Session;

import br.com.senai.entidade.Usuario;
import br.com.senai.util.HibernateUtil;

public class UsuarioDAO {
	
	final Session session = HibernateUtil.getHibernateSession();
	
		public Usuario salvar(Usuario u){
			session.beginTransaction();
			session.save(u);
			session.getTransaction().commit();
			return null;
			
		}
		
		public Usuario apagar(int idSelecionado){
			Usuario usuarioSelecionado = session.load(Usuario.class, idSelecionado);
			session.beginTransaction();
			session.delete(usuarioSelecionado);
			session.getTransaction().commit();
			return null;
			
		}
	


}
