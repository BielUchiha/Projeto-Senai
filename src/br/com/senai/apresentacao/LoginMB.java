package br.com.senai.apresentacao;

import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import br.com.senai.entidade.Usuario;

@ManagedBean
@ViewScoped
public class LoginMB {

	Usuario usuario = new Usuario();
	
	ArrayList<Usuario> listUsuario = new ArrayList<Usuario>();
	
	private int idSelecionado;

	public int getIdSelecionado() {
		return idSelecionado;
	}

	public void setIdSelecionado(int idSelecionado) {
		this.idSelecionado = idSelecionado;
	}

	public Usuario getUsuario() {
		if(usuario == null){
			usuario = new Usuario();
		}
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public ArrayList<Usuario> getListUsuario() {
		return listUsuario;
	}

	public void setListUsuario(ArrayList<Usuario> listUsuario) {
		this.listUsuario = listUsuario;
	}
	
	public void cadastrar(){		
		usuario.setIdUsuario(listUsuario.size()+1);	
		listUsuario.add(usuario);
		usuario = new Usuario();
		
	}
	
	public void excluir(){
		Usuario u = new Usuario();
		u.setIdUsuario(idSelecionado);
		listUsuario.remove(u);
		
	}
	
	
	
	
	
	
	
}
